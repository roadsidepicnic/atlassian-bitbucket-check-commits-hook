package ru.roadsidepicnic.atlassian.bitbucket.hook;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.repository.*;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.bitbucket.util.PagedIterable;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class CheckCommitsHook implements PreReceiveRepositoryHook {
    private static final Logger log = LogManager.getLogger(CheckCommitsHook.class);
    private static final PageRequestImpl PAGE_REQUEST = new PageRequestImpl(0, 100);

    private final RefService refService;
    private final CommitService commitService;

    public CheckCommitsHook(RefService refService, CommitService commitService) {
        this.commitService = commitService;
        this.refService = refService;
    }

    /**
     * Disables regular commits on branches
     */
    @Override
    public boolean onReceive(
            @Nonnull RepositoryHookContext context,
            @Nonnull Collection<RefChange> refChanges,
            @Nonnull HookResponse hookResponse
    ) {
        ArrayList<CommitValidator> commitValidators = new ArrayList<>();

        String authorEmailRegex = context.getSettings().getString("author_email_regex");
        if (authorEmailRegex != null) {
            commitValidators.add(new AuthorEmailRegexValidator(authorEmailRegex.trim()));
        }

        Map<Commit, ArrayList<String>> errors = new HashMap<>();

        for (RefChange refChange : refChanges) {
            if (refChange.getType() == RefChangeType.DELETE) {
                continue;
            }

            for (Commit commit : getPagedChanges(context.getRepository(), refChange)) {
                log.debug(String.format("Checking commit: %s", commit.getId()));
                for (CommitValidator commitValidator : commitValidators) {
                    try {
                        commitValidator.validate(commit);
                    } catch (ValidationException e) {
                        if (!errors.containsKey(commit)) {
                            errors.put(commit, new ArrayList<>());
                        }
                        errors.get(commit).add(e.getMessage());
                        log.debug(String.format("Checking commit failed: %s - %s", commit.getId(), e.getMessage()));
                    }
                }
            }
        }

        if (errors.size() > 0) {
            printErrors(errors, hookResponse);

            return false;
        }

        return true;
    }

    public Iterable<Commit> getPagedChanges(final Repository repository, final RefChange refChange) {
        log.debug(String.format("getBranchesRefs: %s", String.join("; ", getBranchRefs(repository))));

        return new PagedIterable<>(pageRequest -> {
            return commitService.getCommitsBetween(
                    new CommitsBetweenRequest.Builder(repository)
                            .exclude(getBranchRefs(repository))
                            .include(refChange.getToHash())
                            .build(),
                    pageRequest
            );
        }, PAGE_REQUEST);
    }

    private static void printErrors(Map<Commit, ArrayList<String>> errors, @Nonnull HookResponse hookResponse) {
        hookResponse.err().println(header());
        hookResponse.err().println("Next commits failed checks:");

        for (Map.Entry<Commit, ArrayList<String>> entry : errors.entrySet()) {
            Commit commit = entry.getKey();
            Iterable<String> commit_errors = entry.getValue();

            hookResponse.err().println(String.format(
                "> Commit %s '%s':",
                commit.getDisplayId(),
                commit.getMessage()
            ));

            for (String error : commit_errors) {
                hookResponse.err().println(String.format("  - %s", error));
            }

        }

        hookResponse.err().println(footer());
    }

    public Iterable<String> getBranchRefs(Repository repository) {
        PagedIterable<Branch> branches = new PagedIterable<>(pageRequest -> {
            return refService.getBranches(
                    new RepositoryBranchesRequest.Builder(repository).build(),
                    pageRequest
            );
        }, PAGE_REQUEST);

        return StreamSupport.stream(branches.spliterator(), false)
                .map(branch -> branch != null ? branch.getId() : null)
                .filter(input -> input != null && input.startsWith("refs/heads/"))
                .collect(Collectors.toList());
    }

    private static String header() {
        return StringUtils.rightPad("", 100, "=") + "\n" +
            "   ___ _               _        ___                          _ _                           _    \n" +
            "  / __\\ |__   ___  ___| | __   / __\\___  _ __ ___  _ __ ___ (_) |_ ___    /\\  /\\___   ___ | | __\n" +
            " / /  | '_ \\ / _ \\/ __| |/ /  / /  / _ \\| '_ ` _ \\| '_ ` _ \\| | __/ __|  / /_/ / _ \\ / _ \\| |/ /\n" +
            "/ /___| | | |  __/ (__|   <  / /__| (_) | | | | | | | | | | | | |_\\__ \\ / __  / (_) | (_) |   < \n" +
            "\\____/|_| |_|\\___|\\___|_|\\_\\ \\____/\\___/|_| |_| |_|_| |_| |_|_|\\__|___/ \\/ /_/ \\___/ \\___/|_|\\_\\\n" +
            StringUtils.leftPad("by Roadside Picnic (http://roadsidepicnic.ru)", 100, " ") + "\n"
        ;
    }

    private static String footer() {
        return StringUtils.rightPad("", 100, "=") + "\n";
    }
}
