package ru.roadsidepicnic.atlassian.bitbucket.hook;

import com.atlassian.bitbucket.commit.Commit;

public class AuthorEmailRegexValidator implements CommitValidator {
    private String regex;

    public AuthorEmailRegexValidator(String regex) {
        this.regex = regex;
    }

    public void validate(Commit commit) throws ValidationException {
        if (!commit.getAuthor().getEmailAddress().matches(regex)) {
            throw new ValidationException(String.format(
                "Author email `%s` does not match regex /%s/",
                commit.getAuthor().getEmailAddress(),
                regex
            ));
        }
    }

}
