package ru.roadsidepicnic.atlassian.bitbucket.hook;

public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}
