package ru.roadsidepicnic.atlassian.bitbucket.hook;

import com.atlassian.bitbucket.commit.Commit;

public interface CommitValidator {
    void validate(Commit commit) throws ValidationException;
}
